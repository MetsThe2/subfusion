# For each image in 'thermal.pkl', write rgb image to 'gen.pkl' at
# same (subject, seq, frame) indices.

import torch
from torch.autograd import Variable
from batch import *
from rgbdt import *
from time import time

SPLIT_TYPE = 'subject' # Change with model!
out_name = 'tc_subject_orig_12'
name = 'augmented/tc_subject/orig/tc_subject_12_best'
net = torch.load(MODEL_ROOT + name + '.th')

train_tuples = read_seqs_index_tuples(SPLIT_TYPE, 'train')
val_tuples = read_seqs_index_tuples(SPLIT_TYPE, 'val')
test_tuples = read_seqs_index_tuples(SPLIT_TYPE, 'test')

gen_train, gen_val, gen_test = 1, 1, 1
splits = []
if gen_train: splits.append(train_tuples)
if gen_val:   splits.append(val_tuples)
if gen_test:  splits.append(test_tuples)

in_tensor = standardize_input(read_mod_data('thermal'), train_tuples)
in_s = in_tensor.size()
out_s = torch.Size((in_s[0], in_s[1], in_s[2], 3, in_s[4], in_s[5]))
out_tensor = torch.FloatTensor(out_s) # RGB has 3 channels.
out_tensor.zero_()

batch_size = 150 # Max that fits in GPU memory
net.eval()       # Gen batch size can be much bigger than training batch size.

start_time = time()
for tuples in splits:
    print("Current split progress:")
    first = 0
    while first < len(tuples):
        end = min(first + batch_size, len(tuples))
        in_batch = index_tensor_tuples(in_tensor, tuples[first:end]).cuda()
        out_batch = net.forward(Variable(in_batch, volatile=True)).cpu()
        for i in range(first, end):
            out_tensor[tuples[i]] = out_batch.data[i - first]
        if first % 2000 < batch_size:
            print(str(round((100.0 * end) / len(tuples))) + '%')
        first = end
    print('')
with open(OUT_ROOT + 'gen/' + out_name + '_gen.pkl', 'wb') as f:
    pkl.dump(unstandardize_rgb(out_tensor), f)
print("Elapsed:", round(time() - start_time, 1), 'seconds', start_time)