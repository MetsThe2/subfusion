Modifiable parameters are near the top or right after the last function definition in each file.

To initialize data:

 + In `rgbdt.py`, select `IN_ROOT` directory, which has RGB and thermal data as individual `.jpg` images.
 
 + Run `python3 rgbdt.py`.

To train a generator (e.g. FusionNet):

 + Select whether generator should generalize to new frames or subjects with `split_type`.

 + Set `is_orig` to `True` in `fusion.py` to replicate FusionNet (as closely as I could figure out from their paper). Otherwise modify hyperparameters as necessary.

 + Run `python3 fusion.py`. Generator will be saved in `model/`.

To generate rgb images:

 + Select split type model was trained with, model path and output name in `gen.py`.

 + Run `python3 gen.py`.

To view generated images:

 + In `im.py`, select same generator name as in `gen.py`.

 + Select frame range, subject and sequence.

 + Select result directory.

 + Run `python3 im.py`.

To train face classifier:

 + In `vgg.py`, set `is_train` to `True`.

 + Modify hyperparameters as necessary.

 + Run `python3 vgg.py`. Classifier will be saved in `model/`.

To test classification on generated images:

 + In `vgg.py`, put generator names in `test_names`.

 + Select trained classifier path.

 + Set `is_train` to `False`.

 + Run `python3 vgg.py`.

 + **NB:** Check that testing split type corresponds to generator split type.

Best generator comparison is at the top of `fusion.py` and `vgg.py`.