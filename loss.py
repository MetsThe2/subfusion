import torch as th
import torch.nn as nn
from math import sqrt

def RMSE():
    def rmse(output, target):
        return rmse.mse(output, target).sqrt()
    rmse.mse = nn.MSELoss()
    return rmse

def frob_norm(tn):
    return sqrt((tn * tn).sum()) # elemwise mul

def tn_L1(tn):
    return sqrt(tn.abs().sum())

def get_mat_nonorthonormality(mat): # TODO: `mat.t().mm(mat)` instead?
    mat_norm = tn_L1 # TODO: `frob_norm` instead?
    return mat_norm(mat.mm(mat.t()) - th.eye(mat.size()[0]).cuda())

def to_mat(tn):
    return tn.view(tn.size()[0], tn[0].numel())

def add_orthogonal_reg(loss, net, scale):
    reg, num = 0, 0
    for m in net.modules():
        if isinstance(m, nn.Conv2d):
            w = m.weight.data
            reg += get_mat_nonorthonormality(to_mat(w))
            num += 1 # TODO: Add `w.numel()` instead?
    #print("ORTHO REG sum, avg, tot", reg, reg/num, scale * reg/num)
    loss += scale * (reg / num)
    return loss