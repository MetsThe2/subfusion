import matplotlib.pyplot as plt
from batch import *
from save import *

def show_tn(tn):
    arr = to_channels_last((tn.numpy() + 1) * 0.5)
    if arr.shape[-1] == 1: arr = arr.reshape(arr.shape[:-1])
    plt.imshow(arr)
    plt.show()

def plot_losses(model_losses, names, first_epoch):
    for i, epoch_losses in enumerate(model_losses):
        x = range(first_epoch, len(epoch_losses))
        epoch_losses = epoch_losses[first_epoch :]
        train, val = zip(*epoch_losses)
        plt.plot(x, train)
        plt.scatter(x, val, label=names[i])
        plt.axvline(x=100)
        plt.axhline(y=0.16)
        plt.axhline(y=0.15)
        plt.axhline(y=0.10)
        plt.axhline(y=0.09)
        if i == len(model_losses) - 1:
            plt.legend()
            plt.show()
        else:
            plt.draw() # Loop through all losses before showing.

if __name__ == '__main__':
    names = [
        'tc_subject_90',
        'augmented/rz_subject/nocp/rz_subject_150'
        #'augmented/rz_frame/nocp/rz_frame_88_best',
        #'augmented/sp_frame/nocp/sp_frame_117_best',
        #'augmented/tc_frame/nocp/tc_frame_69_best'
    ]
    first_epoch = 0 # Show losses of this and later epochs

    model_losses = []
    for name in names:
        epoch_losses, best_val_loss = load_losses(name)
        print(epoch_losses[-1])
        model_losses.append(epoch_losses)
    plot_losses(model_losses, names, first_epoch)