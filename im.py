from PIL import Image
import pickle as pkl
import numpy as np
from rgbdt import *
from batch import *
import os

gen_name   = 'rz_frame_nocp_88'
result_dir = './result/' + gen_name + '/'
subject    = 26
seq        = 0 # pose, expr, illum <--> 0, 1, 2
frames_in  = (46, 50)

def get_data_PIL(name):
    a = read_mod_data(name).transpose(0, 1, 2, 4, 5, 3) # uint8 data
    is_grayscale = (a.shape[-1] == 1)
    return a.reshape(a.shape[:-1]) if is_grayscale else a

def name_from_indices(indices): # indices=(subject,seq,frame)
    return '_'.join(map(str, indices))

def save_thermal_frame(thermal, indices):
    path = result_dir + name_from_indices(indices) + '_thermal.jpg'
    Image.fromarray(thermal[indices], 'L').save(path)

def save_rgb_frame(rgb_data, indices, is_gen=False):
    path = result_dir + name_from_indices(indices) + ('.jpg' if is_gen else '_rgb.jpg')
    Image.fromarray(rgb_data[indices], 'RGB').save(path)

gen_path = 'gen/' + gen_name + '_gen'
indices = [subject, seq, None]
thermal = get_data_PIL('thermal')
rgb     = get_data_PIL('rgb')
gen     = get_data_PIL(gen_path)
for frame_index in range(frames_in[0], frames_in[1]):
    indices[2] = frame_index
    save_thermal_frame(thermal, tuple(indices))
    save_rgb_frame(rgb, tuple(indices))
    save_rgb_frame(gen, tuple(indices), is_gen=True)