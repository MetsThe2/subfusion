import os
import tensorflow
import numpy as np
import keras
from keras.engine import Model
from keras.optimizers import SGD, RMSprop
from keras.layers import Flatten, Dense
from keras.regularizers import l2 # TODO: Use this?
from keras_vggface.vggface import VGGFace
from random import shuffle
from rgbdt import *
from batch import *

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' # Don't print TensorFlow debug info.

'''
generator_name          pose, expression, illumination
frame or subject split (cross entropy, top-1 accuracy)

---------------------- frame

raw vgg_b16_4
train (0.0015, 0.9996)   (0.0000, 1.0000)   (0.0001, 1.0000)    
val   (0.1471, 0.9667)   (0.0512, 0.9833)   (0.0008, 1.0000)
test  (0.0260, 0.9938)   (0.0019, 0.9996)   (0.0005, 1.0000)    

rz_frame_nocp_88
train (0.6488, 0.8094)   (0.2539, 0.9393)   (0.1219, 0.9556)
val   (0.8998, 0.7367)   (0.2574, 0.9367)   (0.2247, 0.9067)
test  (0.5931, 0.8694)   (0.0842, 0.9752)   (0.0249, 0.9919)    


sp_frame_nocp_117
train (0.6454, 0.8496)   (0.1837, 0.9585)   (0.0778, 0.9739)  
val   (0.7432, 0.7900)   (0.2897, 0.9400)   (0.3255, 0.9300)    
test  (0.8164, 0.8182)   (0.2085, 0.9581)   (0.0971, 0.9702)


tc_frame_nocp_69
train (0.8004, 0.8303)   (0.5250, 0.9132)   (0.1939, 0.9440)    
val   (1.0349, 0.7733)   (0.6191, 0.8967)   (0.2829, 0.9467)
test  (0.9092, 0.8023)   (0.5683, 0.9101)   (0.2373, 0.9345)    


tc_frame_orig_60
train (0.6648, 0.8406)   (0.1300, 0.9538)   (0.0569, 0.9842)
val   (1.0813, 0.7833)   (0.2023, 0.9433)   (0.2740, 0.9133)
test  (0.8388, 0.8155)   (0.1549, 0.9488)   (0.0764, 0.9725)    

---------------------- subject

raw vgg_b16_4
train (0.0145, 0.9962)   (0.0000, 1.0000)   (0.0003, 1.0000)    
val   (0.0181, 0.9963)   (0.0000, 1.0000)   (0.0000, 1.0000)
test  (0.0336, 0.9924)   (0.0069, 0.9986)   (0.0005, 1.0000)


rz_subject_nocp_56            # rz overfits more than tc?
train (0.4372, 0.8788)   (0.0021, 0.9996)   (0.0215, 0.9948)
val   (5.9154, 0.4444)   (5.9851, 0.6241)   (6.1805, 0.5481)
test  (2.8973, 0.6243)   (2.1185, 0.7019)   (2.0159, 0.7195)


sp_subject_nocp_31
train (1.4179, 0.6897)   (0.5519, 0.8583)   (0.3388, 0.8966)
val   (4.3456, 0.4722)   (5.7458, 0.6259)   (5.0314, 0.5889)
test  (3.4109, 0.5114)   (3.3174, 0.6305)   (3.7704, 0.5805)    


tc_subject_nocp_29
train (1.1639, 0.7361)   (0.5481, 0.8865)   (0.2614, 0.9290)
val   (5.4292, 0.4333)   (5.7547, 0.6278)   (5.8134, 0.6148)
test  (2.9409, 0.5710)   (2.4256, 0.6719)   (2.7213, 0.6233) 


tc_subject_orig_12
train (2.1863, 0.5952)   (1.8433, 0.7437)   (1.4941, 0.7621)    
val   (4.6417, 0.3944)   (5.3579, 0.4870)   (5.0279, 0.4111) 
test  (2.8979, 0.5562)   (2.5502, 0.6419)   (2.8520, 0.6457)  

'''

def get_model(input_width, input_height, class_num):
    input_shape = (input_width, input_height, 3)
    default_model = VGGFace(include_top=False, input_shape=input_shape)
    x = default_model.get_layer('pool5').output
    x = Flatten(name='flatten')(x)
    #x = Dense(512, activation='relu', name='fc6', kernel_initializer='he_normal')(x)
    x = Dense(512, activation='relu', name='fc7', kernel_initializer='he_normal')(x)
    x = Dense(class_num, activation='softmax', name='fc8')(x)
    model = Model(default_model.input, x)
    optimizer = SGD(lr=1e-2, momentum=0.9, decay=1e-3, nesterov=False)
    #optimizer = RMSprop(lr=1e-4)
    metrics = ['accuracy']
    model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=metrics)
    return model

def targets_from_tuples(tuples):
    subject_indices = np.array([t[0] for t in tuples])
    return keras.utils.to_categorical(subject_indices, SUBJECT_NUM)

def index_array_tuples(full, tuples):
    shape = tuple([len(tuples)] + list(full.shape[len(tuples[0]):]))
    part = np.empty(shape, dtype=full.dtype)
    for i, t in enumerate(tuples): part[i] = full[t]
    return part # Flattened first `len(tuples[0])` axises

def get_tuples_avg_metrics(model, tuples, rgb_data):
    t = 0
    eval_batch_size = 60 # Can be bigger because no gradients are stored.
    sums = [0, 0]
    batch_num = 0
    while t < len(tuples):
        batch_tuples, t = get_batch_tuples(t, eval_batch_size, tuples)
        batch_inputs = index_array_tuples(rgb_data, batch_tuples)
        batch_targets = targets_from_tuples(batch_tuples)
        metrics = model.test_on_batch(batch_inputs, batch_targets)
        for i in range(len(metrics)): sums[i] += metrics[i]
        batch_num += 1
    return [s / batch_num for s in sums] # list of avgs

def print_metrics(metrics):
    print("Cross entropy:", metrics[0], "\tAccuracy:", metrics[1])

def train_on_batches(model, train_tuples, rgb_data, batch_size):
    shuffle(train_tuples)
    t, batch_num = 0, 0
    train_metrics = [0,0]
    while t < len(train_tuples):
        batch_tuples, t = get_batch_tuples(t, batch_size, train_tuples)
        batch_inputs = index_array_tuples(rgb_data, batch_tuples)
        batch_targets = targets_from_tuples(batch_tuples)
        metrics = model.train_on_batch(batch_inputs, batch_targets)
        print_metrics(metrics)
        for i in range(len(metrics)): train_metrics[i] += metrics[i]
        batch_num += 1
        print("Done:", str(round(t / len(train_tuples) * 100, 1)) + '%')
    train_metrics = tuple(s/batch_num for s in train_metrics)
    return train_metrics

def train_epoch(model, train_tuples, rgb_data, batch_size):
    train_batch = index_array_tuples(rgb_data, train_tuples)
    train_targets = targets_from_tuples(train_tuples)
    history = model.fit(x=train_batch, y=train_targets, batch_size=batch_size)
    return history

def train_vgg(model, train_tuples, val_tuples, rgb_data, new_model_path):
    batch_size = 32 # vgg man batch size 32
    epoch_num = 3
    use_fit = True
    epoch_metrics = []
    for epoch in range(epoch_num):
        print('\nEpoch:', epoch)
        if use_fit:
            history = train_epoch(model, train_tuples, rgb_data, batch_size)
        else:
            train_metrics = train_on_batches(model, train_tuples, rgb_data, batch_size)
            print("\nTrain avg:")
            print_metrics(train_metrics)
        
        print("Validating...")
        val_metrics = get_tuples_avg_metrics(model, val_tuples, rgb_data)
        print_metrics(val_metrics)
        if not use_fit:
            epoch_metrics.append((train_metrics, val_metrics))
            with open('model/vgg_epoch_metrics.txt', 'w') as f:
                print(epoch_metrics, file=f)
        if new_model_path is not None:
            model.save_weights(new_model_path + "_" + str(epoch+1) + '.pkl')
    return model, epoch_metrics

def test_vgg(model, generator_name, split_type, rgb_data):
    print("\n\nCross entropy, accuracy:", str(generator_name), split_type)
    print('\t', '\t\t\t\t\t'.join(SEQ_NAMES))
    for split_name in SPLIT_NAMES:
        if split_name in ('test',): continue

        metrics = []
        for seq_index in range(len(SEQ_NAMES)):
            tuples = read_index_tuples(split_type, split_name, seq_index)
            metrics.append(get_tuples_avg_metrics(model, tuples, rgb_data))
        s = ''
        for i,m in enumerate(metrics):
            s += '({0:.4f}, {1:.4f})\t'.format(m[0], m[1])
        print(split_name, s)

final_subject = ['rz_subject_nocp_56', 'sp_subject_nocp_31',
                 'tc_subject_nocp_29', 'tc_subject_orig_12']
final_frame = ['rz_frame_nocp_88', 'sp_frame_nocp_117',
               'tc_frame_nocp_69', 'tc_frame_orig_60']
raw_rgb = ['rgb_subject', 'rgb_frame']
test_names = raw_rgb
is_train = False

old_model_path = 'model/vgg/vgg_b16_4'
new_model_path = 'model/vgg/vgg_b16'
model = get_model(OUT_WIDTH, OUT_HEIGHT, SUBJECT_NUM)
if old_model_path: model.load_weights(old_model_path + '.pkl')

if is_train:
    split_type = 'frame' # VGG can't possibly generalize to new subjects.
    train_tuples = read_seqs_index_tuples(split_type, 'train')
    val_tuples = read_seqs_index_tuples(split_type, 'val')
    rgb_data = read_mod_data('rgb')
    train_vgg(model, train_tuples, val_tuples, rgb_data, new_model_path)
else:
    for generator_name in test_names:
        parts = generator_name.split('_')
        split_type = parts[1]
        if parts[0] == 'rgb': # actual rgb data
            rgb_data = read_mod_data('rgb')
        else:                 # generated data
            mod_name = 'gen/' + generator_name + '_gen'
            rgb_data = read_mod_data(mod_name)
        rgb_data = to_channels_last(uint_to_float_pixel(rgb_data))
        test_vgg(model, generator_name, split_type, rgb_data)
