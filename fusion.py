import torch
import torch.nn as nn
import torch.optim as optim
from torch.autograd import Variable
import numpy as np
from random import shuffle
from model import FusionNet
from batch import *
from rgbdt import *
from save  import *
from loss  import *
from time import time

# ----------------------------------------------- FINAL frame

# rz frame nocp dp=0.1 or=1e-4 pat=15 ld=0.2 wd=1e-3 b=20 mo=0.9 d=8
# initial learning rate 0.1 due to smaller batch size
# [([0.1], 0),
#  ([0.02], 72),    (88 best 0.087135)
#  ([0.004], 103),
#  ([0.0008], 119),
#  ([0.00016], 135)]

# sp frame nocp dp=0.1 or=1e-4 pat=15 ld=0.2 wd=1e-3 b=160 mo=0.9 e=8
# [([1.0], 0),
#  ([0.2], 76),
#  ([0.04], 108), (117 best 0.085854)
#  ([0.008], 132),
#  ([0.0016], 148)]

# tc frame nocp dp=0.1 or=1e-4 pat=15 ld=0.2 wd=1e-3 b=160 mo=0.9 d=8
# [([1.0], 0),
#  ([0.2], 40),
#  ([0.04], 60),   (69 best 0.088472)
#  ([0.008], 84),
#  ([0.0016], 100),
#  ([0.00032], 116),
#  ([6.4e-05], 132),
#  ([1.28e-05], 148),
#  ([2.56e-06], 164),
#  ([5.12e-07], 180),
#  ([1.024e-07], 196)]

# tc frame orig init=vgg dp=0 or=0 pat=15 ld=0.2 wd=0 b=20 mo=0.9 d=8
# [([0.1], 0),
#  ([0.02], 46),    (60 best 0.086426)
#  ([0.004], 75),
#  ([0.0008], 91),
#  ([0.00016], 107),
#  ([3.2e-05], 123),
#  ([6.4e-06], 139)]

# ------------------------------------------ FINAL subject

# rz subject nocp dp=0.1 or=1e-4 pat=15 ld=0.2 wd=1e-3 b=20 mo=0.9 d=8
# [([0.1], 0),      (56 best 0.14881)
#  ([0.02], 71),
#  ([0.004], 87),
#  ([0.0008], 103),
#  ([0.00016], 119),
#  ([3.2e-05], 135)]

# sp subject nocp dp=0.1 or=1e-4 pat=15 ld=0.2 wd=1e-3 b=20 mo=0.9 e=8
# [([0.1], 0),      (31 best 0.15368)
#  ([0.02], 46),
#  ([0.004], 62),
#  ([0.0008], 78),
#  ([0.00016], 94),
#  ([3.2e-05], 110),
#  ([6.4e-06], 126),
#  ([1.28e-06], 142)]

# tc subject nocp dp=0.1 or=1e-4 pat=15 ld=0.2 wd=1e-3 b=20 mo=0.9 d=8
# [([0.1], 0),      (29 best 0.15224)
#  ([0.02], 44),
#  ([0.004], 60),
#  ([0.0008], 76),
#  ([0.00016], 92),
#  ([3.2e-05], 108),
#  ([6.4e-06], 124),
#  ([1.28e-06], 140)]

# tc subject orig init=vgg dp=0 or=0 pat=15 ld=0.2 wd=0 b=20 mo=0.9 d=8
# [([0.1], 0),        (12 best 0.15110)
#  ([0.02], 27),
#  ([0.004], 43),
#  ([0.0008], 59),
#  ([0.00016], 75),
#  ([3.2e-05], 91),
#  ([6.4e-06], 107),
#  ([1.28e-06], 123),
#  ([2.56e-07], 139)]

# -----------------------------------------------------------------------

# best sp sj d_4/dp_1e-1: b=20, lr=1e-1, lr39=1e-2, wd=1e-3, or=1e-4, mo=0.9
# 2nd  sp sj e_8: b=160, dp=1e-1, lr=1e-0, wd=1e-4, or=1e-6, mo=0.9
# best tc sj d_8: b=160, dp=1e-1, lr=1e-0, wd=1e-4, or=1e-6, mo=0.9
# best sp fm e_8: b=160, dp=1e-1, lr=1e-0, wd=1e-3, or=1e-4, mo=0.9
# best tc fm d_8: TODO

def get_avg(vals):
    return sum(vals) / len(vals)

def get_lrs(optimizer):
    return [float(pg['lr']) for pg in optimizer.param_groups]

is_orig = True # Replicate original FusionNet.
dc_type = 'tc' if is_orig else 'sp' # decoding convolutional block type
split_type = 'subject'
train_tuples = read_seqs_index_tuples(split_type, 'train')
val_tuples = read_seqs_index_tuples(split_type, 'val')
in_tensor = standardize_input(read_mod_data('thermal'), train_tuples)
target_tensor = standardize_rgb(read_mod_data('rgb'))

load_epoch = 39
if load_epoch is None:
    dp = 0 if is_orig else 0.1
    net = nn.DataParallel(FusionNet(dp, dc_type, 1, 8, 3, is_orig=is_orig)).cuda()
    epoch_losses, best_loss = [], None
else:
    is_best = True
    name = get_model_name(dc_type, split_type, load_epoch, is_best)
    net = load_net(name)
    epoch_losses, best_loss = load_losses(name)

loss_fn = RMSE()
optimizer = optim.SGD(
    net.parameters(),
    lr=1e-2,
    momentum=0.9,
    dampening=0.9,
    weight_decay=1e-3,
    nesterov=False
)
# Weight decay is for preventing weight size from growing
# without bound with batchnorm, not for regularization.

ortho_reg = 0 if is_orig else 1e-4
scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, patience=15, factor=0.2)
transform = get_transform(shift=0.05, rot=5, zoom=0.05)
epoch_num = 150
batch_size = 20 # Technically, each epoch one batch might be smaller than this.
print_frame_period = 1000 # Print after this number of training images
save_epoch_period = 5

def get_loss(tuples, first_index, losses, is_eval):
# If is_eval==True, network activations are discarded, so the
# returned loss can't be backpropagated.
    in_batch, target_batch, first_index = get_batch(first_index, batch_size, tuples,
                                                    in_tensor, target_tensor)
    if not is_eval:
        transform_batch(transform, in_batch, target_batch) # Executed on CPU
    out_batch = net.forward(Variable(in_batch, volatile=is_eval).cuda())
    loss = loss_fn(out_batch, Variable(target_batch, volatile=is_eval).cuda())
    loss = add_orthogonal_reg(loss, net, ortho_reg)
    losses.append(loss.data[0])
    return loss, first_index

prev_lrs = []
first_epoch = 0 if load_epoch is None else load_epoch + 1
for epoch in range(first_epoch, epoch_num):
    print("Epoch:", epoch)
    start_time = int(time())

    net.train()
    shuffle(train_tuples)
    t, train_losses = 0, []
    while t < len(train_tuples):
        optimizer.zero_grad()
        loss, t = get_loss(train_tuples, t, train_losses, False)
        loss.backward()
        optimizer.step()
        if (t - 1) % print_frame_period < batch_size:
            finished_batch_num = int(t / batch_size)
            print('Batch ' + str(finished_batch_num) + ' train loss:', loss.data[0])
    
    print('Validating...')
    net.eval()
    t, val_losses = 0, []
    while t < len(val_tuples):
        loss, t = get_loss(val_tuples, t, val_losses, True)
    epoch_losses.append((get_avg(train_losses), get_avg(val_losses)))
    scheduler.step(epoch_losses[-1][1]) # Avg val loss
    lrs = get_lrs(optimizer)
    if len(prev_lrs) == 0 or prev_lrs[-1][0] != lrs:
        prev_lrs.append((lrs, epoch))
    print('========= Learning rates:', prev_lrs)
    best_loss = maybe_save(net, epoch_losses, save_epoch_period, best_loss, dc_type, split_type)
    end_time = int(time())
    print("Epoch time:", end_time - start_time, 'seconds', start_time, end_time)
print('Trained SubFusionNet.')