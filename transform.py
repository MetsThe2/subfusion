from torchsample import transforms
from rgbdt import *
from batch import *
from plot  import *

ts = read_seqs_index_tuples('frame', 'train')
rgb = standardize_input(read_mod_data('rgb'), ts)
thermal = standardize_input(read_mod_data('thermal'), ts)
tn = thermal[ts[0]]
show_tn(tn)

transform = transforms.Compose([
    transforms.RandomAffine(
        translation_range=0.05,
        rotation_range=5,
        zoom_range=(0.95, 1.05)
    ),
    transforms.RandomFlip()
])
for i in range(10):
    show_tn(transform(tn))