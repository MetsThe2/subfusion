import torch as th
import torch.nn as nn
import torch.nn.init as init
from torch.autograd import Variable
from loss import *
from plot import *

out_ch = 4
im_num, in_ch, height, width = in_shape = (1, 1, 4, 4)
x = th.Tensor(th.Size(in_shape))
x.bernoulli_(th.Tensor([0.5]).expand_as(x)).mul_(0.25).add_(0.25)
m = nn.Conv2d(in_ch, out_ch, kernel_size=3, stride=1, padding=1)
m.eval()

m.bias.data.fill_(0)
w = m.weight.data
print('w', w.size())
w.normal_()
print('ortho', get_mat_nonorthonormality(to_mat(w)))

gain = 2
init.orthogonal(w, gain=gain)
print(w[0])
print('ortho', get_mat_nonorthonormality(to_mat(w)))

for i in range(w.size()[0]):
    #w.fill_(0 if i < out_ch/2 else 1)
    w[i] = w[i - (i % 4)]

y = m.forward(Variable(x, volatile=True))
print('y', y.size())
s = nn.PixelShuffle(2).forward(y)
show_tn(x[0])
show_tn(s.data[0])