import torch
import torch.nn as nn
import torch.nn.init as init
from math import sqrt
from block import *

class ConvResConv(nn.Module):

    def __init__(self, in_ch, out_ch, act_fn):
        super(ConvResConv, self).__init__()
        mid_ch = out_ch
        self.in_ch, self.out_ch, = in_ch, out_ch
        self.conv_1 = conv_block(in_ch, mid_ch, act_fn)
        self.conv_2 = conv_block_3(mid_ch, mid_ch, act_fn)
        self.conv_3 = conv_block(mid_ch, out_ch, act_fn)

    def forward(self, x):
        conv_1 = self.conv_1(x)
        conv_2 = self.conv_2(conv_1)
        res = conv_1 + conv_2
        conv_3 = self.conv_3(res)
        return conv_3


class FusionNet(nn.Module):

    def __init__(self, dp, dc_type, in_channel_num, internal_channel_num,
                 out_channel_num, is_orig=False):
        super(FusionNet, self).__init__()
        if is_orig: assert dc_type == 'tc', dc_type
        bdict = {'tc': conv_trans_block, 'sp': conv_subpixel_block, 'rz': conv_resize_block}
        deconv_block = bdict[dc_type] # `dc_type` must be key of `bdict`.
        self.in_ch = in_channel_num
        self.mid_ch = internal_channel_num
        self.out_ch = out_channel_num

        rrelu = (1.0/8.0, 1.0/3.0)
        act_fn = nn.ReLU() if is_orig else nn.RReLU(lower=rrelu[0], upper=rrelu[1])

        # For encoding, we halve the width and height and double the number of
        # channels with each layer to encourage encoding detailed information
        # (many channels) about only a few (small width and height) important
        # (max pooling) locations in the image.

        self.down_1 = ConvResConv(self.in_ch, self.mid_ch, act_fn)
        self.pool_1 = maxpool()
        self.down_2 = ConvResConv(self.mid_ch, self.mid_ch * 2, act_fn)
        self.pool_2 = maxpool()
        self.down_3 = ConvResConv(self.mid_ch * 2, self.mid_ch * 4, act_fn)
        self.pool_3 = maxpool()
        self.down_4 = ConvResConv(self.mid_ch * 4, self.mid_ch * 8, act_fn)
        self.pool_4 = maxpool()

        # The bridge layer lets the nn prepare for decoding. This is the
        # layer which has the most channels, so use 2d dropout here.

        self.bridge = ConvResConv(self.mid_ch * 8, self.mid_ch * 16, act_fn)
        if is_orig: assert dp == 0, dp
        self.dp2d = nn.Dropout2d(p=dp)

        # During decoding the network gradually spreads the information to
        # many pixels and few channels.

        f = (2 if dc_type == 'sp' else 1)
        self.deconv_1 = deconv_block(self.mid_ch * 16, self.mid_ch * 8, act_fn)
        self.up_1 = ConvResConv(self.mid_ch * 8, self.mid_ch * 8//f, act_fn)
        self.deconv_2 = deconv_block(self.mid_ch * 8//f, self.mid_ch * 4, act_fn)
        self.up_2 = ConvResConv(self.mid_ch * 4, self.mid_ch * 4//f, act_fn)
        self.deconv_3 = deconv_block(self.mid_ch * 4//f, self.mid_ch * 2, act_fn)
        self.up_3 = ConvResConv(self.mid_ch * 2, self.mid_ch * 2//f, act_fn)
        self.deconv_4 = deconv_block(self.mid_ch * 2//f, self.mid_ch, act_fn)
        self.up_4 = ConvResConv(self.mid_ch, self.mid_ch, act_fn)

        # Use two final layers to output the correct number of channels in the
        # correct color space and numerical range. 

        self.out = nn.Conv2d(self.mid_ch, self.out_ch, kernel_size=3, stride=1, padding=1)
        self.out_2 = nn.Tanh()

        # Initialize weights and biases of kernels in convolutional layers.

        if is_orig: # Use vgg initialization.
            for m in self.modules():
                if isinstance(m, nn.Conv2d):
                    n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                    m.weight.data.normal_(0, sqrt(2. / n))
                    if m.bias is not None:
                        m.bias.data.zero_()
                elif isinstance(m, nn.BatchNorm2d):
                    m.weight.data.fill_(1)
                    m.bias.data.zero_()
        else:
            avg_slope = rrelu[0] * 0.5 + rrelu[1] * 0.5
            gain = init.calculate_gain('leaky_relu', param=avg_slope)

            for m in self.modules():
                if isinstance(m, nn.Conv2d):
                    init.orthogonal(m.weight.data, gain=gain)
                    m.bias.data.fill_(1e-5) # Close to zero, but breaks symmetry.
                elif isinstance(m, nn.BatchNorm2d):
                    m.weight.data.fill_(1)
                    m.bias.data.zero_()

            artifactless_sp = False # Destroys orthogonality (future work: can this be avoided?).
            if artifactless_sp: # Init so pixels in each PixelShuffle square are equal.
                sp_layers = [self.deconv_1, self.deconv_2, self.deconv_3, self.deconv_4]
                for layer in sp_layers:
                    for m in layer.modules():
                        if not isinstance(m, nn.Conv2d): continue

                        w = m.weight.data # Weights of kernels before PixelShuffle
                        for i in range(w.size()[0]):
                            upscale = 2
                            area = upscale * upscale
                            r = i % area
                            if r != 0: # TODO: REM cond if `w[i]=w[i]` is fast.
                                w[i] = w[i - r] # Delete frac (area-1)/area of kernels.

    def forward(self, x):

        down_1 = self.down_1(x)
        pool_1 = self.pool_1(down_1)
        down_2 = self.down_2(pool_1)
        pool_2 = self.pool_2(down_2)
        down_3 = self.down_3(pool_2)
        pool_3 = self.pool_3(down_3)
        down_4 = self.down_4(pool_3)
        pool_4 = self.pool_4(down_4)

        bridge = self.bridge(pool_4)
        dp2d = self.dp2d(bridge)

        deconv_1 = self.deconv_1(dp2d)
        skip_1 = (deconv_1 + down_4)/2
        up_1 = self.up_1(skip_1)
        deconv_2 = self.deconv_2(up_1)
        skip_2 = (deconv_2 + down_3)/2
        up_2 = self.up_2(skip_2)
        deconv_3 = self.deconv_3(up_2)
        skip_3 = (deconv_3 + down_2)/2
        up_3 = self.up_3(skip_3)
        deconv_4 = self.deconv_4(up_3)
        skip_4 = (deconv_4 + down_1)/2
        up_4 = self.up_4(skip_4)

        out = self.out(up_4)
        out = self.out_2(out)
        #out = torch.clamp(out, min=-1, max=1)

        return out