# Write '[rgb,thermal].pkl'.
# Write '[frame,subject]_[train,val,test]_[pose,expr,illum].txt'.
# '*.pkl' shape is (subject,seq,frame,channel,height,width).

import cv2
import numpy as np
import pickle as pkl

SEQ_NAMES = ('pose', 'expr', 'illum')
SPLIT_NAMES = ('train', 'val', 'test')
MOD_NAMES = ('rgb', 'thermal')
MOD_ACRONYMS = ('RGB', 'T')

FRAME_NUM = 100
TRAIN_FRAME_NUM = 45
VAL_FRAME_NUM = 5
TEST_FRAME_NUM = 50
assert(TRAIN_FRAME_NUM + VAL_FRAME_NUM + TEST_FRAME_NUM == FRAME_NUM)

SUBJECT_NUM = 51
TRAIN_SUBJECT_NUM = 25
VAL_SUBJECT_NUM = 5
TEST_SUBJECT_NUM = 21
assert(TRAIN_SUBJECT_NUM + VAL_SUBJECT_NUM + TEST_SUBJECT_NUM == SUBJECT_NUM)

OUT_WIDTH = 128
OUT_HEIGHT = OUT_WIDTH
OUT_ROOT = './data/'
IN_ROOT = '../../Roman/datasets/RGB-D-T/'
MODEL_ROOT = './model/'

def get_seq_path(subject_index, seq_index):
    subject_path = IN_ROOT + str(subject_index + 1) + '/Cap/'
    return subject_path + str(seq_index + 1) + '/'

def get_roi_path(seq_path, mod_index):
    return seq_path + 'Groundtruth' + MOD_ACRONYMS[mod_index] + '.txt'

def get_im_path(seq_path, mod_index, frame_index):
    dir_path = seq_path + 'Sync' + MOD_ACRONYMS[mod_index] + '/'
    return dir_path + str(frame_index + 1) + '.jpg'

def get_index_tuples_path(split_type, split_name, seq_index):
    name = '_'.join((split_type, split_name, SEQ_NAMES[seq_index])) + '.txt'
    return OUT_ROOT + name

def read_index_tuples(split_type, split_name, seq_index):
    with(open(get_index_tuples_path(split_type, split_name, seq_index))) as f:
        tuples = []
        for line in f:
            s = line.strip()
            if len(s) > 0:
                tuples.append(tuple(map(int, s.split(' '))))
        return tuples

def read_seqs_index_tuples(split_type, split_name):
    tuples = []
    for seq_index in range(len(SEQ_NAMES)):
        tuples.extend(read_index_tuples(split_type, split_name, seq_index))
    return tuples

def write_index_tuples(tuples, split_type, split_name, seq_index):
    with open(get_index_tuples_path(split_type, split_name, seq_index), 'w') as f:
        for t in tuples:
            assert(len(t) == 3) # subject, seq, frame
            print(' '.join(map(str, t)), file=f)

def get_frame_split_index(frame_index): # 0 train, 1 val, 2 test
    return ((frame_index >= TRAIN_FRAME_NUM + VAL_FRAME_NUM) +
            (frame_index >= TRAIN_FRAME_NUM))

def get_subject_split_index(subject_index):
    return ((subject_index >= TRAIN_SUBJECT_NUM + VAL_SUBJECT_NUM) +
            (subject_index >= TRAIN_SUBJECT_NUM))

def write_splits():
    for seq_index in range(len(SEQ_NAMES)):
        frame_splits, subject_splits = [], []
        for i in range(len(SPLIT_NAMES)):
            frame_splits.append([])
            subject_splits.append([])

        # NB: Doing `frame_splits = ([],) * len(SPLIT_NAMES)` instead
        # would be wrong! It wouldn't create a new list at each index,
        # but a shallow copy of the same list, so modifying a split at
        # one index would also modify all the others.

        for subject_index in range(SUBJECT_NUM):
            for frame_index in range(FRAME_NUM):
                t = (subject_index, seq_index, frame_index)
                fsi = get_frame_split_index(frame_index)
                ssi = get_subject_split_index(subject_index)
                frame_splits[fsi].append(t)
                subject_splits[ssi].append(t)

        for split_index in range(len(SPLIT_NAMES)):
            name = SPLIT_NAMES[split_index]
            write_index_tuples(frame_splits[split_index], 'frame', name, seq_index)
            write_index_tuples(subject_splits[split_index], 'subject', name, seq_index)

def read_mod_data(mod_name): # TODO: Rename to read_pkl_data?
    with open(OUT_ROOT + mod_name + '.pkl', 'rb') as f:
        return np.load(f)

def write_mod_data(data, mod_index): # TODO: Delegate to `write_pkl_data(data, pkl_name)`?
    file_path = OUT_ROOT + MOD_NAMES[mod_index] + '.pkl'
    print(file_path)
    with open(file_path, 'wb') as f:
        pkl.dump(data, f)

def bgr_to_rgb(im):
    im[0,:], im[2,:] = im[2,:], im[0,:].copy()

def get_mod_data(mod_index):
    is_rgb = MOD_NAMES[mod_index] == 'rgb'
    channel_num = (3 if is_rgb else 1)
    shape = (SUBJECT_NUM, len(SEQ_NAMES), FRAME_NUM, channel_num, OUT_HEIGHT, OUT_WIDTH)
    mod_data = np.empty(shape, dtype=np.uint8)
    for subject_index in range(SUBJECT_NUM):
        print("Subject index:", subject_index)
        for seq_index in range(len(SEQ_NAMES)):
            seq_path = get_seq_path(subject_index, seq_index)
            with open(get_roi_path(seq_path, mod_index)) as f:
                roi_rows = f.readlines()
            for frame_index in range(FRAME_NUM):
                im_path = get_im_path(seq_path, mod_index, frame_index)
                flag = (cv2.IMREAD_COLOR if is_rgb else cv2.IMREAD_GRAYSCALE)
                im = cv2.imread(im_path, flag)
                x, y, h, w = map(int, roi_rows[frame_index].strip().split(','))
                im = cv2.resize(im[y:y+h, x:x+w], (OUT_HEIGHT, OUT_WIDTH))
                if is_rgb:
                    im = np.transpose(im, (2, 0, 1)) # (y,x,channel) to (channel,y,x)
                    bgr_to_rgb(im) # OpenCV uses BGR.
                mod_data[subject_index][seq_index][frame_index] = im
    return mod_data

def write_data():
    for mod_index in range(len(MOD_NAMES)):
        print("Modality:", MOD_NAMES[mod_index])
        data = get_mod_data(mod_index)
        print(data.shape)
        write_mod_data(data, mod_index)
    write_splits()

if __name__ == '__main__':
    write_data()

# TODO: Input max desired two rotations and four translations,
#       output max possible (should usually be equal to max desired) and
#       partial crop size to get minimum of max possible and max desired
#       (to reduce pkl size), with 1 pixel of margin for rounding errors.
#       (TODO: max combination of rotation and translation?)
#       At training time, enlarge bounding box vertically by
#           floor((max_down_translation + max_up_translation) / 2)
#       and analogously horizontally, then translate the bounding box by
#       up to the same amount, so the total movement is <= the maximum if
#       the maximum is even and < the maximum if it is odd.
#       (Don't rotate and translate at the same time to stay within limits?)
# TODO: Pixelwise noise (uniform/gaussian?)