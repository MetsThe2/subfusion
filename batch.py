import torch
import numpy as np
from torchsample import transforms

def uint_to_float_pixel(data):
    return (data.astype(np.float32) * (2.0 / 255.0)) - 1.0

def float_to_uint_pixel(data):
    return (data + 1.0) * 127.5

def standardize_input(in_data, train_tuples):
    # TODO: Calculate mean and sd from in_data[train_tuples]?
    standardized = uint_to_float_pixel(in_data)
    return torch.FloatTensor(standardized)

def standardize_rgb(rgb_data):
    train_tuples = None # Works for now with fixed mean and sd.
    return standardize_input(rgb_data, train_tuples)

def unstandardize_rgb(rgb_tensor):
    return float_to_uint_pixel(rgb_tensor).numpy().astype(np.uint8)

def to_channels_last(rgb_data): # Transposes numpy array.
    ax_num = len(rgb_data.shape)
    perm = list(range(ax_num - 3)) + [ax_num - 2, ax_num - 1, ax_num - 3]
    return np.transpose(rgb_data, tuple(perm))

def index_tensor_tuples(full, tuples):
    shape = [len(tuples)]
    shape.extend(full.shape[len(tuples[0]):])
    part = torch.Tensor(torch.Size(shape))
    for i, t in enumerate(tuples):
        part[i] = full[t]
    return part

def get_batch_tuples(first_index, max_size, tuples):
    end_index = min(first_index + max_size, len(tuples))
    assert end_index > first_index, end_index
    return tuples[first_index : end_index], end_index 

def get_batch(first_index, max_size, tuples, in_tensor, target_tensor=None):
    index_with, end_index = get_batch_tuples(first_index, max_size, tuples)
    in_batch = index_tensor_tuples(in_tensor, index_with)
    if target_tensor is None:
        return in_batch, end_index

    target_batch = index_tensor_tuples(target_tensor, index_with)
    return in_batch, target_batch, end_index

def get_transform(shift=None, rot=None, zoom=0):
    return transforms.Compose([
        transforms.RandomAffine(
            translation_range=shift,
            rotation_range=rot,
            zoom_range=(1 - zoom, 1 + zoom)
        ),
        transforms.RandomFlip()
    ])

def maybe_expand_gray(img_batch):
    assert len(img_batch.size()) == 4
    is_gray = img_batch.size()[1] == 1
    return img_batch.expand((-1, 3, -1, -1)) if is_gray else img_batch

def transform_batch(transform, in_batch, target_batch):
    n = in_batch.size()[0]
    assert n == target_batch.size()[0], n
    for i in range(n):
        in_batch[i], target_batch[i] = transform(in_batch[i], target_batch[i])

''' REM
def get_transform(original_shape, transformed_shape, max_shifts, max_rot, max_noise):
    assert False

def augment_batch(transform, batch_data, transformed_shape):
    batch_input_num = batch_data.size()[0]
    size = torch.Size([batch_input_num] + list(transformed_shape))
    transformed = torch.Tensor(size)
    for i in range(batch_input_num):
        transformed[i] = transform(batch_data[i])
    return transformed
'''