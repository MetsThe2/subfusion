import torch
import torch.nn as nn

def conv_block(in_ch, out_ch, act_fn):
    return nn.Sequential(
        nn.Conv2d(in_ch, out_ch, kernel_size=3, stride=1, padding=1),
        act_fn,
        nn.BatchNorm2d(out_ch)
    )

def conv_trans_block(in_ch, out_ch, act_fn):
    upscale = 2
    return nn.Sequential(
        nn.ConvTranspose2d(in_ch, out_ch, kernel_size=3, stride=upscale, padding=1, output_padding=1),
        act_fn,
        nn.BatchNorm2d(out_ch)
    )

def conv_subpixel_block(in_ch, out_ch, act_fn):
    upscale = 2
    return nn.Sequential(
        nn.Conv2d(in_ch, out_ch * upscale * upscale, kernel_size=3, stride=1, padding=1),
        nn.PixelShuffle(upscale),
        act_fn,
        nn.BatchNorm2d(out_ch)
    )

def conv_resize_block(in_ch, out_ch, act_fn):
    upscale = 2
    mode = 'nearest' # not bilinear (distill)
    return nn.Sequential(
        nn.Upsample(scale_factor=upscale, mode=mode),
        nn.Conv2d(in_ch, out_ch, kernel_size=3, stride=1, padding=1),
        act_fn,
        nn.BatchNorm2d(out_ch)
    )

def conv_block_3(in_ch, out_ch, act_fn):
    return nn.Sequential(
        conv_block(in_ch, out_ch, act_fn),
        conv_block(out_ch, out_ch, act_fn),
        nn.Conv2d(out_ch, out_ch, kernel_size=3, stride=1, padding=1),
        nn.BatchNorm2d(out_ch) # No act_fn at the end
    )

def maxpool():
    return nn.MaxPool2d(kernel_size=2, stride=2, padding=0)