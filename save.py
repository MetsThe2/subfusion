import torch
from rgbdt import *

def get_model_name(dc_type, split_type, epoch, is_best):
    assert dc_type in ('tc', 'sp', 'rz'), dc_type
    s = '_'.join((dc_type, split_type, str(epoch)))
    return s + '_best' if is_best else s

def get_net_path(model_name):
    return MODEL_ROOT + model_name + '.th'

def get_losses_path(model_name):
    return MODEL_ROOT + model_name + '_losses.txt'

def load_net(model_name):
    return torch.load(get_net_path(model_name))

def load_losses(model_name):
    losses, best = [], None
    with open(get_losses_path(model_name)) as f:
        for line in f:
            losses.append(tuple(map(float, line.strip().split(' '))))
            if best is None or best > losses[-1][1]: best = losses[-1][1]
    return losses, best

def save_net(net, model_name):
    torch.save(net, get_net_path(model_name))

def save_losses(losses, model_name):
    with open(get_losses_path(model_name), 'w') as f:
        for t in losses:
            print(' '.join(map(str, t)), file=f)

def maybe_save(net, epoch_losses, save_epoch_period, best_loss, dc_type, split_type):
    assert len(epoch_losses) > 0
    print('Avg train, val loss:', epoch_losses[-1])
    val_loss = epoch_losses[-1][1]
    is_best = (best_loss is None or val_loss < best_loss)
    if is_best or len(epoch_losses) % save_epoch_period == 0:
        name = get_model_name(dc_type, split_type, len(epoch_losses), is_best)
        save_net(net, name)
        save_losses(epoch_losses, name)
    if not is_best:
        print("-------> Better val loss before:", best_loss)
    return val_loss if is_best else best_loss